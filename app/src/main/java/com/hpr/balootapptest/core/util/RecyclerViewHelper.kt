package com.hpr.balootapptest.core.util

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import timber.log.Timber

fun initFullRecyclerViewAdapterDataObserver(callback: () -> Unit): RecyclerView.AdapterDataObserver {

    return object : RecyclerView.AdapterDataObserver() {

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            callback()
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
            callback()
        }

        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            callback()
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            callback()
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
            callback()
        }
    }
}
