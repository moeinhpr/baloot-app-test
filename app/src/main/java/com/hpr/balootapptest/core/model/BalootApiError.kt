package com.hpr.balootapptest.core.model

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class BalootApiError(
    val status: String = "",
    val message: String = "",
    val code: String = ""
)
