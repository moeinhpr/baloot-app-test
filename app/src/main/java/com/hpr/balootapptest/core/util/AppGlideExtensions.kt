package com.hpr.balootapptest.core.util

import android.content.Context
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.hpr.balootapptest.R

object AppGlideExtensions {

    fun default(): RequestOptions {
        return RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    }

    fun circlePlaceHolder(): RequestOptions {
        return RequestOptions()
            .transform(CircleCrop())
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    }

    fun mediumRadius(context: Context): RequestOptions {
        return RequestOptions()
            .transform(
                CenterCrop(),
                RoundedCorners(
                    context.resources.getDimensionPixelSize(R.dimen.radius_medium)
                )
            )
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    }

    fun mediumTopRadius(context: Context): RequestOptions {
        return RequestOptions()
            .transform(
                CenterCrop(),
                GranularRoundedCorners(
                    context.resources.getDimension(R.dimen.radius_medium),
                    context.resources.getDimension(R.dimen.radius_medium),
                    0f,
                    0f
                )
            )
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    }
}
