package com.hpr.balootapptest.core.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.hpr.balootapptest.features.article.data.ArticleDao
import com.hpr.balootapptest.features.article.data.entities.ArticleListEntity

/**
 * Main database description.
 */
@Database(
    entities = [
        ArticleListEntity::class
    ],
    version = 1,
    exportSchema = false
)

abstract class BalootDb : RoomDatabase() {

    abstract fun articleDao(): ArticleDao

}
