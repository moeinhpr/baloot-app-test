package com.hpr.balootapptest.core.model

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class BalootApiDetailsResponse<out T>(
    override val status: String = "",
    override val message: String = "",
    override val code: String = "",
    val response: T,
) : BalootApiResponse()
