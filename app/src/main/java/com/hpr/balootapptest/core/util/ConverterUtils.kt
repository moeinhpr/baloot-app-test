package com.hpr.balootapptest.core.util

import com.hpr.balootapptest.core.di.BalootJsonAdapterFactory
import com.squareup.moshi.Moshi

object ConverterUtils {

    val jsonConverter: Moshi = Moshi.Builder()
        .add(BalootJsonAdapterFactory.INSTANCE)
        .build()
}
