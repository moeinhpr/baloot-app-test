package com.hpr.balootapptest.core.util.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.hpr.balootapptest.features.main.ui.MainNavigationController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.hpr.balootapptest.R

abstract class BaseBottomSheetDialogFragment<V : ViewDataBinding> :
    BottomSheetDialogFragment() {

    private var _binding: V? = null
    val binding get() = _binding!!

    val navigationController by lazy { (requireActivity() as MainNavigationController) }

    var saveView = true

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        dialog.setOnShowListener { dialogs ->
            val d = dialogs as BottomSheetDialog
            val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
            if (bottomSheet != null) {
                BottomSheetBehavior.from(bottomSheet).apply {
                    state = BottomSheetBehavior.STATE_EXPANDED
                    skipCollapsed = true
                }
            }
        }
        dialog.window?.let {
            if (showKeyboard) {
                it.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            } else {
                it.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            }
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (saveView) {
            if (_binding == null)
                _binding = DataBindingUtil.inflate(
                    inflater,
                    layoutId,
                    container,
                    false
                )
            return binding.root
        } else {
            _binding = DataBindingUtil.inflate(
                inflater,
                layoutId,
                container,
                false
            )
            return binding.root
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    abstract val showKeyboard: Boolean

    @get:LayoutRes
    abstract val layoutId: Int

    open fun onScrollToTop() {}
}
