package com.hpr.balootapptest.core.model

abstract class BalootApiResponse {
    abstract val status: String
    abstract val message: String
    abstract val code: String
}
