package com.hpr.balootapptest.core.api

import com.hpr.balootapptest.core.exception.Exceptions
import com.hpr.balootapptest.core.model.ApiResult
import com.hpr.balootapptest.core.model.BalootApiResponse
import com.hpr.balootapptest.core.util.errorParser
import retrofit2.Response
import javax.inject.Inject

open class BaseRemoteDataSource {

    @Inject
    lateinit var urls: ApiUrlHelper

    protected fun <T> checkApiResult(response: Response<T>): ApiResult<T> {
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                (body as BalootApiResponse?)?.let {
                    return if (it.status != "error")
                        ApiResult.Success(body)
                    else ApiResult.Error(
                        Exceptions.RemoteDataSourceException(
                            it.status,
                            it.message,
                            it.code,
                            response.code()
                        )
                    )
                }
            }
        }
        val error = errorParser(response.errorBody()?.string())
        return ApiResult.Error(
            Exceptions.RemoteDataSourceException(
                error.status,
                error.message,
                error.code,
                response.code()
            )
        )
    }
}
