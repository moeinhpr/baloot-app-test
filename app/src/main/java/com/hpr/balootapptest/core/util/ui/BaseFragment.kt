package com.hpr.balootapptest.core.util.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.hpr.balootapptest.features.main.ui.MainNavigationController

abstract class BaseFragment<V : ViewDataBinding> : Fragment() {

    private var _binding: V? = null
    val binding get() = _binding!!

    val navigationController by lazy { (requireActivity() as MainNavigationController) }

    var saveView = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (saveView) {
            if (_binding == null)
                _binding = DataBindingUtil.inflate(
                    inflater,
                    layoutId,
                    container,
                    false
                )
            return binding.root
        } else {
            _binding = DataBindingUtil.inflate(
                inflater,
                layoutId,
                container,
                false
            )
            return binding.root
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    @get:LayoutRes
    abstract val layoutId: Int

    open fun onScrollToTop() {}
}
