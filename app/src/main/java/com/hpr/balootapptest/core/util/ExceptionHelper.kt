package com.hpr.balootapptest.core.util

import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.exception.Exceptions
import timber.log.Timber

object ExceptionHelper {

    fun getError(exception: Exceptions): ErrorView {

        val serverErrorMessage: String?
        val message: Int
        val errorCode: String
        val unauthorized: Boolean
        Timber.d(exception.toString())

        when (exception) {
            is Exceptions.IOException -> {
                serverErrorMessage = null
                message = R.string.error_server
                errorCode = ""
                unauthorized = false

            }
            is Exceptions.NetworkConnectionException -> {
                serverErrorMessage = null
                message = R.string.error_connection
                errorCode = ""
                unauthorized = false

            }
            is Exceptions.LocalDataSourceException -> {
                serverErrorMessage = null
                message = R.string.error_general
                errorCode = ""
                unauthorized = false

            }
            is Exceptions.RemoteDataSourceException -> {
                serverErrorMessage =
                    if (exception.message.isNullOrEmpty()) null else exception.message
                message = R.string.error_server
                errorCode = exception.code
                unauthorized = unauthorized(exception.responseCode)

            }
            else -> {
                serverErrorMessage = null
                message = R.string.error_general
                errorCode = ""
                unauthorized = false

            }
        }
        return ErrorView(
            serverErrorMessage = serverErrorMessage,
            message = message,
            errorCode = errorCode,
            unauthorized = unauthorized,

            )
    }

    private fun unauthorized(responseCode: Int): Boolean {
        return responseCode == 401
    }

    data class ErrorView(
        val serverErrorMessage: String?,
        val message: Int,
        val errorCode: String,
        val unauthorized: Boolean,
    )
}
