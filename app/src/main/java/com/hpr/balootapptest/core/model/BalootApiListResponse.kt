package com.hpr.balootapptest.core.model

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class BalootApiListResponse<out T>(
    override val status: String = "",
    override val message: String = "",
    override val code: String = "",
    val articles: List<T> = ArrayList(),
) : BalootApiResponse()
