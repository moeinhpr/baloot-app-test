package com.hpr.balootapptest.core.util.binding

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.hpr.balootapptest.core.util.loadImageWithGlide
import com.hpr.balootapptest.core.util.loadImageWithGlideMediumRadius
import java.util.*

@BindingAdapter("imageUrl")
fun bindImage(imageView: AppCompatImageView, url: String?) {
    imageView.loadImageWithGlide(url)
}

@BindingAdapter("imageUrlMediumRadius")
fun bindImageMediumRadius(imageView: AppCompatImageView, url: String?) {
    imageView.loadImageWithGlideMediumRadius(url)
}

@BindingAdapter("imageUrlMediumRadius")
fun bindImageMediumRadius(imageView: AppCompatImageView, drawable : Int?) {
    imageView.loadImageWithGlideMediumRadius(drawable)
}

@BindingAdapter("setVisibilityGone")
fun bindVisibilityGone(view: View, isGone: Boolean) {
    view.visibility = if (isGone) View.GONE else View.VISIBLE
}

@BindingAdapter("setVisibilityInvisible")
fun bindVisibilityInvisible(view: View, isInvisible: Boolean) {
    view.visibility = if (isInvisible) View.INVISIBLE else View.VISIBLE
}

@BindingAdapter("setTimeStampToDate")
fun bindTimeStampToDate(textView: AppCompatTextView, timeStamp: Long) {
    textView.text = Date(timeStamp).toString()
}

