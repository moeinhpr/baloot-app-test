package com.hpr.balootapptest.core.util


interface NetworkCallback {
    fun refresh()
    fun retry() = refresh()
}
