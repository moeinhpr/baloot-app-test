package com.hpr.balootapptest.core.api

import com.hpr.balootapptest.BuildConfig
import javax.inject.Inject

@Suppress("PrivatePropertyName", "PropertyName")
class ApiUrlHelper @Inject constructor() {

    init {
        update()
    }

    private lateinit var API_URL: String


    private fun update() {
        API_URL = BuildConfig.API_URL

    }
}
