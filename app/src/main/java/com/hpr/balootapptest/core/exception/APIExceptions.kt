package com.hpr.balootapptest.core.exception

enum class APIExceptions(val value: String) {
    AuthenticateError("authenticateError"),
    ValidationError("validationError");
}
