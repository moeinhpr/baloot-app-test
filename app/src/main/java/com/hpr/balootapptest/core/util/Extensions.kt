package com.hpr.balootapptest.core.util

import android.app.Activity
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.hpr.balootapptest.core.di.GlideApp
import java.text.SimpleDateFormat
import java.util.*

fun ImageView.loadImageWithGlide(url: String?) {
    url?.let {
        GlideApp.with(this)
            .load(it)
            .transition(withCrossFade())
            .into(this)
    }
}

fun ImageView.loadImageWithGlideMediumRadius(url: String?) {
    url?.let {
        GlideApp.with(this)
            .load(it)
            .apply(AppGlideExtensions.mediumRadius(context))
            .transition(withCrossFade())
            .into(this)
    }
}

fun ImageView.loadImageWithGlideMediumRadius(drawable : Int?) {
    drawable?.let {
        GlideApp.with(this)
            .load(it)
            .apply(AppGlideExtensions.mediumRadius(context))
            .transition(withCrossFade())
            .into(this)
    }
}


fun Activity.showLongToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Activity.showLongToast(resourceId: Int) {
    Toast.makeText(this, resourceId, Toast.LENGTH_LONG).show()
}

fun Activity.showShortToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Activity.showShortToast(resourceId: Int) {
    Toast.makeText(this, resourceId, Toast.LENGTH_SHORT).show()
}

fun String.dateToTimeStamp(): Long {
    return try {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        val date = inputFormat.parse(this)
        date?.time ?: System.currentTimeMillis()
    } catch (exception: Exception) {
        0L
    }
}



