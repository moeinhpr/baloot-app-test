package com.hpr.balootapptest.core.util.ui

import androidx.fragment.app.Fragment

object ParentFragment {
    private var parentFragment: Fragment? = null
    private var parentFragmentMap = HashMap<Any, Fragment?>()
    fun get(): Fragment? {
        return parentFragment
    }

    fun set(fragment: Fragment?) {
        parentFragment = fragment
    }

    fun getByType(type: Any): Fragment? {
        return parentFragmentMap[type]
    }

    fun setByType(type: Any, fragment: Fragment?) {
        parentFragmentMap[type] = fragment
    }

    fun clearMap() {
        parentFragmentMap = HashMap<Any, Fragment?>()
    }
}
