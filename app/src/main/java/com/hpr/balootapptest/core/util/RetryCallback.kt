package com.hpr.balootapptest.core.util

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}
