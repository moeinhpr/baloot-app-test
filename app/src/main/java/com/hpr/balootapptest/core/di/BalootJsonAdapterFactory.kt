package com.hpr.balootapptest.core.di

import com.squareup.moshi.JsonAdapter
import se.ansman.kotshi.KotshiJsonAdapterFactory

@KotshiJsonAdapterFactory
abstract class BalootJsonAdapterFactory : JsonAdapter.Factory {
    companion object {
        val INSTANCE: BalootJsonAdapterFactory = KotshiBalootJsonAdapterFactory
    }
}
