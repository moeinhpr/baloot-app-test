package com.hpr.balootapptest.features.main.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hpr.balootapptest.core.util.showLongToast
import com.hpr.balootapptest.core.util.showShortToast
import com.hpr.balootapptest.core.util.ui.BaseActivity
import com.hpr.balootapptest.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*

@AndroidEntryPoint
class MainActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemReselectedListener,
    MainNavigationController {

    lateinit var binding: ActivityMainBinding

    private val mainNavigationManager by lazy {
        MainNavigationManager(
            this
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initValue()
        initView(savedInstanceState)
        initObservation()
    }

    private fun initValue() {

    }

    private fun initView(savedInstanceState: Bundle?) {


        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        if (savedInstanceState == null)
            mainNavigationManager.initTabManager()
        mainNavigationManager.initDestinationChangedListener()

        binding.bnvMain.apply {
            setOnNavigationItemSelectedListener(this@MainActivity)
            setOnNavigationItemReselectedListener(this@MainActivity)
        }
    }

    private fun initObservation() {

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mainNavigationManager.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        mainNavigationManager.onRestoreInstanceState(savedInstanceState)
    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mainNavigationManager.onActivityResult(requestCode, resultCode, data)
    }

    override fun supportNavigateUpTo(upIntent: Intent) {
        mainNavigationManager.supportNavigateUpTo()
    }

    override fun onBackPressed() {
        mainNavigationManager.onBackPressed()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        mainNavigationManager.switchTab(menuItem.itemId)
        return true
    }

    override fun onNavigationItemReselected(menuItem: MenuItem) {
        mainNavigationManager.scrollToTop()
        mainNavigationManager.clearStack()
    }

    override fun navController(): NavController {
        return mainNavigationManager.navController()
    }

    override fun navigate(direction: NavDirections) {
        mainNavigationManager.navigate(direction)
    }

    override fun navigateSinglePage(direction: NavDirections, finish: Boolean) {
        mainNavigationManager.navigateSinglePage(direction, finish)
    }

    override fun clearStack(tag: MainNavigationTag) {
        mainNavigationManager.clearStack(tag)
    }

    override fun goBack(tag: MainNavigationTag, number: Int) {
        mainNavigationManager.goBack(tag, number)
    }

    override fun switchTab(tag: MainNavigationTag) {
        mainNavigationManager.switchTab(tag)
    }

    override fun showLongMessage(resourceId: Int) {
        showLongToast(resourceId)
    }

    override fun showLongMessage(message: String) {
        showLongToast(message)
    }

    override fun showShortMessage(resourceId: Int) {
        showShortToast(resourceId)
    }

    override fun showShortMessage(message: String) {
        showShortToast(message)
    }

    override fun showRemoteMessage(serverErrorMessage: String?, errorMessage: Int) {
        if (serverErrorMessage == null) {
            if (errorMessage != 0)
                showLongMessage(errorMessage)
        } else showLongMessage(serverErrorMessage)
    }
}
