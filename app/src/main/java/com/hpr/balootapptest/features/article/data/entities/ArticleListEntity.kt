package com.hpr.balootapptest.features.article.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import se.ansman.kotshi.JsonSerializable

@Entity
@JsonSerializable
data class ArticleListEntity(
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,
    val author : String = "",
    val title : String = "",
    val description : String = "",
    val urlToImage : String = "",
    val publishedAt : Long = 0L,
    val content : String = "",
)
