package com.hpr.balootapptest.features.profile.ui

import android.os.Bundle
import android.view.View
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.ui.BaseBottomSheetDialogFragment
import com.hpr.balootapptest.databinding.SheetAboutMeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileAboutMeSheet :
    BaseBottomSheetDialogFragment<SheetAboutMeBinding>() {

    override val showKeyboard: Boolean
        get() = false

    override val layoutId: Int
        get() = R.layout.sheet_about_me

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initValue()
        initView()
        initObservation()
    }

    private fun initValue() {}

    private fun initView() {}

    private fun initObservation() {}

}
