package com.hpr.balootapptest.features.profile.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.ui.BaseFragment
import com.hpr.balootapptest.databinding.FragmentProfileBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>(), View.OnClickListener {


    override val layoutId: Int
        get() = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initValue()
        initView()
        initObservation()
    }

    private fun initValue() {}

    private fun initView() {
        binding.apply {
            ivGithubProfileFragment.setOnClickListener(this@ProfileFragment)
            ivLinkedinProfileFragment.setOnClickListener(this@ProfileFragment)
            btnAboutMeProfileFragment.setOnClickListener(this@ProfileFragment)
        }

    }

    private fun initObservation() {}

    private fun intentToBrowser(link : Int){
        val url = getString(link)
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    override fun onClick(v: View?) {
        v?.let {
            when (it.id) {
                binding.ivGithubProfileFragment.id -> {
                    intentToBrowser(R.string.link_github)
                }

                binding.ivLinkedinProfileFragment.id -> {
                    intentToBrowser(R.string.link_linkedin)
                }

                binding.btnAboutMeProfileFragment.id -> {
                    navigationController.navigate(
                        ProfileFragmentDirections.actionProfileFragmentToAboutMeSheet()
                    )
                }
            }
        }
    }

}
