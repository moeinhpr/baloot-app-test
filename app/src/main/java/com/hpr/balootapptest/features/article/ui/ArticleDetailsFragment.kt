package com.hpr.balootapptest.features.article.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.*
import com.hpr.balootapptest.core.util.ui.BaseFragment
import com.hpr.balootapptest.databinding.FragmentArticleDetailsBinding
import com.hpr.balootapptest.databinding.FragmentArticleListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ArticleDetailsFragment : BaseFragment<FragmentArticleDetailsBinding>() {

    private val viewModel by viewModels<ArticleDetailsViewModel>()

    private val params by navArgs<ArticleDetailsFragmentArgs>()

    override val layoutId: Int
        get() = R.layout.fragment_article_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initValue()
        initView()
        initObservation()
    }

    private fun initValue() {
        viewModel.emitArticleId(params.articleId)
    }

    private fun initView() {
        binding.viewToolbarArticleDetails.apply {
            ivBack.setOnClickListener { navigationController.goBack() }
            tvPageTitle.text = getString(R.string.label_articles)
        }
    }

    private fun initObservation() {

        viewModel.articleDetails.observe(viewLifecycleOwner, {
            binding.articleDetails = it
        })


    }

}
