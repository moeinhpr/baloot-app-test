package com.hpr.balootapptest.features.article.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.hpr.balootapptest.databinding.ItemArticleBinding
import com.hpr.balootapptest.databinding.ItemLoadingMoreStateBinding
import com.hpr.balootapptest.features.article.data.entities.ArticleListEntity

class ArticleListAdapter(private val itemClick: (ArticleListEntity) -> Unit) :
    ListAdapter<ArticleListEntity, RecyclerView.ViewHolder>(

        object : DiffUtil.ItemCallback<ArticleListEntity>() {
            override fun areItemsTheSame(
                oldItem: ArticleListEntity,
                newItem: ArticleListEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ArticleListEntity,
                newItem: ArticleListEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    ) {
    companion object {

        const val VIEW_NORMAL = 0
        const val VIEW_LOADING = 1
    }

    private var isNetworkStateLoading: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            VIEW_NORMAL -> {
                val view = ItemArticleBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                ArticleListViewHolder(view)
            }
            else -> {
                val view = ItemLoadingMoreStateBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                ArticleListLoadingViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArticleListViewHolder)
            holder.onBind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return if (isNetworkStateLoading && position == itemCount - 1) VIEW_LOADING
        else VIEW_NORMAL
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (isNetworkStateLoading) 1 else 0
    }

    fun setLoadingState(isNetworkStateLoading: Boolean) {

        val previousState = this.isNetworkStateLoading

        this.isNetworkStateLoading = isNetworkStateLoading

        if (previousState != isNetworkStateLoading) {
            if (isNetworkStateLoading)
                notifyItemInserted(super.getItemCount())
            else
                notifyItemRemoved(super.getItemCount())
        }
    }

    inner class ArticleListViewHolder(private val binding: ItemArticleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(item: ArticleListEntity) {
            binding.item = item

            binding.root.setOnClickListener {
                itemClick.invoke(item)
            }
        }
    }


    class ArticleListLoadingViewHolder(binding: ItemLoadingMoreStateBinding) :
        RecyclerView.ViewHolder(binding.root)
}
