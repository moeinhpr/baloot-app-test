package com.hpr.balootapptest.features.main.ui

enum class MainNavigationTag {
    Default,
    Empty,
    Article,
    Profile
}
