package com.hpr.balootapptest.features.main.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.ui.BaseFragment
import com.hpr.balootapptest.core.util.ui.safeNavigate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainNavigationManager(private val mainActivity: MainActivity) {

    private val startDestinations = mapOf(
        R.id.navigation_emptyFragment to R.id.navigation_emptyFragment,
        R.id.navigation_articleListFragment to R.id.navigation_articleListFragment,
        R.id.navigation_profileFragment to R.id.navigation_profileFragment
    )

    private var currentNavigationId: Int = R.id.navigation_emptyFragment
    private var currentNavigationController: NavController? = null

    private var navEmptyControllerIsCreated = false
    private val navEmptyController by lazy {
        mainActivity.findNavController(R.id.nav_empty).apply {
            graph = navInflater.inflate(R.navigation.main_navigation).apply {
                startDestination = startDestinations.getValue(R.id.navigation_emptyFragment)
            }
        }
    }
    private var navArticleControllerIsCreated = false
    private val navArticleController by lazy {
        mainActivity.findNavController(R.id.nav_articleList).apply {
            graph = navInflater.inflate(R.navigation.main_navigation).apply {
                startDestination = startDestinations.getValue(R.id.navigation_articleListFragment)
            }
        }
    }

    private var navProfileControllerIsCreated = false
    private val navProfileController by lazy {
        mainActivity.findNavController(R.id.nav_profile).apply {
            graph = navInflater.inflate(R.navigation.main_navigation).apply {
                startDestination = startDestinations.getValue(R.id.navigation_profileFragment)
            }
        }
    }

    private val emptyContainer: View by lazy { mainActivity.binding.containerEmpty }
    private val articleContainer: View by lazy { mainActivity.binding.containerArticles }
    private val profileContainer: View by lazy { mainActivity.binding.containerProfile }
    private val viewDividerMain: View by lazy { mainActivity.binding.viewDividerMain }
    private val bnvMain: BottomNavigationView by lazy { mainActivity.binding.bnvMain }

    fun initTabManager() {
        currentNavigationController = navEmptyController
        navigateSinglePage(EmptyFragmentDirections.actionEmptyFragmentToSplashFragment())
    }

    fun initDestinationChangedListener() {
        navEmptyController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.navigation_emptyFragment -> {
                    switchTab(bnvMain.selectedItemId)
                }
                else -> {
                    switchTab(startDestinations.getValue(R.id.navigation_emptyFragment))
                }
            }
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(KEY_EMPTY_NAVIGATION_CREATED, navEmptyControllerIsCreated)
        outState.putBoolean(KEY_ARTICLE_NAVIGATION_CREATED, navArticleControllerIsCreated)
        outState.putBoolean(KEY_PROFILE_NAVIGATION_CREATED, navProfileControllerIsCreated)

        if (navEmptyControllerIsCreated)
            outState.putBundle(KEY_EMPTY_NAVIGATION_STATE, navEmptyController.saveState())
        if (navArticleControllerIsCreated)
            outState.putBundle(KEY_ARTICLE_NAVIGATION_STATE, navArticleController.saveState())
        if (navProfileControllerIsCreated)
            outState.putBundle(KEY_PROFILE_NAVIGATION_STATE, navProfileController.saveState())

        outState.putSerializable(KEY_CURRENT_NAV_ID, currentNavigationId)
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            navEmptyControllerIsCreated = it.getBoolean(KEY_EMPTY_NAVIGATION_CREATED)
            navArticleControllerIsCreated = it.getBoolean(KEY_ARTICLE_NAVIGATION_CREATED)
            navProfileControllerIsCreated = it.getBoolean(KEY_PROFILE_NAVIGATION_CREATED)

            if (navEmptyControllerIsCreated)
                navEmptyController.restoreState(it.getBundle(KEY_EMPTY_NAVIGATION_STATE))
            if (navArticleControllerIsCreated)
                navArticleController.restoreState(it.getBundle(KEY_ARTICLE_NAVIGATION_STATE))
            if (navProfileControllerIsCreated)
                navProfileController.restoreState(it.getBundle(KEY_PROFILE_NAVIGATION_STATE))

            currentNavigationId = it.getSerializable(KEY_CURRENT_NAV_ID) as Int
            switchTab(currentNavigationId)
        }
    }

    @Suppress("DEPRECATION")
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        currentNavigationController?.let {
            val emptyNavHostFragment =
                mainActivity.supportFragmentManager.findFragmentById(R.id.nav_empty) as NavHostFragment?
            emptyNavHostFragment?.let {
                it.childFragmentManager.fragments.forEach { fragment ->
                    fragment.onActivityResult(requestCode, resultCode, data)
                }
            }
            val articleNavHostFragment =
                mainActivity.supportFragmentManager.findFragmentById(R.id.nav_articleList) as NavHostFragment?
            articleNavHostFragment?.let {
                it.childFragmentManager.fragments.forEach { fragment ->
                    fragment.onActivityResult(requestCode, resultCode, data)
                }
            }
            val profileNavHostFragment =
                mainActivity.supportFragmentManager.findFragmentById(R.id.nav_profile) as NavHostFragment?
            profileNavHostFragment?.let {
                it.childFragmentManager.fragments.forEach { fragment ->
                    fragment.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
    }

    fun supportNavigateUpTo() {
        currentNavigationController?.navigateUp()
    }

    fun onBackPressed() {
        currentNavigationController?.let {
            if (it.currentDestination == null || it.currentDestination?.id == R.id.navigation_splashFragment) mainActivity.finish()
            else if (
                it.currentDestination == null || it.currentDestination?.id ==
                startDestinations.getValue(currentNavigationId)
            ) {
                if (it.currentDestination?.id == startDestinations.getValue(R.id.navigation_articleListFragment))
                    mainActivity.finish()
                else {
                    switchToArticleFragment()
                }
            } else it.popBackStack()
        } ?: run {
            mainActivity.finish()
        }
    }

    fun switchTab(tag: MainNavigationTag) {
        when (tag) {
            MainNavigationTag.Default -> {
            }
            MainNavigationTag.Empty -> {
            }
            MainNavigationTag.Article -> switchToArticleFragment()
            MainNavigationTag.Profile -> switchToProfileFragment()
        }
    }

    fun switchTab(tabId: Int) {
        currentNavigationId = tabId

        when (tabId) {
            R.id.navigation_emptyFragment -> {
                currentNavigationController = navEmptyController
                navEmptyControllerIsCreated = true
                invisibleTabContainerExcept(emptyContainer, true)
            }
            R.id.navigation_articleListFragment -> {
                currentNavigationController = navArticleController
                navArticleControllerIsCreated = true
                invisibleTabContainerExcept(articleContainer, false)
            }
            R.id.navigation_profileFragment -> {
                currentNavigationController = navProfileController
                navProfileControllerIsCreated = true
                invisibleTabContainerExcept(profileContainer, false)
            }
        }
    }

    private fun switchToArticleFragment() {
        switchTab(startDestinations.getValue(R.id.navigation_articleListFragment))
        bnvMain.menu.findItem(R.id.navigation_articleListFragment)?.isChecked = true
    }


    private fun switchToProfileFragment() {
        switchTab(startDestinations.getValue(R.id.navigation_profileFragment))
        bnvMain.menu.findItem(R.id.navigation_profileFragment)?.isChecked = true
    }

    fun clearStack(tag: MainNavigationTag = MainNavigationTag.Default) {
        when (tag) {
            MainNavigationTag.Default -> currentNavigationController?.graph?.startDestination?.let {
                currentNavigationController?.popBackStack(
                    it,
                    false
                )
            }
            MainNavigationTag.Empty -> navEmptyController.graph.startDestination.let {
                currentNavigationController?.popBackStack(
                    it,
                    false
                )
            }
            MainNavigationTag.Article -> navArticleController.graph.startDestination.let {
                currentNavigationController?.popBackStack(
                    it,
                    false
                )
            }
            MainNavigationTag.Profile -> navProfileController.graph.startDestination.let {
                currentNavigationController?.popBackStack(
                    it,
                    false
                )
            }
        }
    }

    fun scrollToTop() {
        currentNavigationController?.currentDestination?.id?.let {

            when (it) {

                R.id.navigation_articleListFragment -> {
                    (mainActivity.supportFragmentManager.findFragmentById(R.id.nav_articleList) as NavHostFragment?)?.let { navHostFragment ->
                        navHostFragment.childFragmentManager.fragments.forEach { fragment ->
                            if (fragment is BaseFragment<*>)
                                fragment.onScrollToTop()
                        }
                    }
                }
                R.id.navigation_profileFragment -> {
                    (mainActivity.supportFragmentManager.findFragmentById(R.id.nav_profile) as NavHostFragment?)?.let { navHostFragment ->
                        navHostFragment.childFragmentManager.fragments.forEach { fragment ->
                            if (fragment is BaseFragment<*>)
                                fragment.onScrollToTop()
                        }
                    }
                }
                else -> {
                }
            }
        }
    }

    fun goBack(tag: MainNavigationTag = MainNavigationTag.Default, number: Int = 1) {
        mainActivity.lifecycleScope.launch(Dispatchers.Main) {
            repeat(number) {
                when (tag) {
                    MainNavigationTag.Default -> onBackPressed()
                    MainNavigationTag.Empty -> navEmptyController.popBackStack()
                    MainNavigationTag.Article -> navArticleController.popBackStack()
                    MainNavigationTag.Profile -> navProfileController.popBackStack()
                }
            }
        }
    }

    private fun invisibleTabContainerExcept(container: View, hideNavigation: Boolean) {
        emptyContainer.visibility = View.GONE
        articleContainer.visibility = View.GONE
        profileContainer.visibility = View.GONE

        container.visibility = View.VISIBLE
        if (hideNavigation) {
            viewDividerMain.visibility = View.GONE
            bnvMain.visibility = View.GONE
        } else {
            viewDividerMain.visibility = View.VISIBLE
            bnvMain.visibility = View.VISIBLE
        }

    }

    fun navController(): NavController {
        return currentNavigationController!!
    }

    fun navigate(directions: NavDirections) {
        currentNavigationController?.let {
            safeNavigate(it, directions)
        }
    }

    fun navigateSinglePage(directions: NavDirections, finish: Boolean = false) {
        if (finish) onBackPressed()
        safeNavigate(navEmptyController, directions)
    }

    companion object {
        private const val KEY_EMPTY_NAVIGATION_CREATED = "key_empty_navigation_created"
        private const val KEY_EMPTY_NAVIGATION_STATE = "key_empty_navigation_state"

        private const val KEY_ARTICLE_NAVIGATION_CREATED = "key_home_navigation_created"
        private const val KEY_ARTICLE_NAVIGATION_STATE = "key_home_navigation_state"

        private const val KEY_PROFILE_NAVIGATION_CREATED = "key_filter_navigation_created"
        private const val KEY_PROFILE_NAVIGATION_STATE = "key_filter_navigation_state"

        private const val KEY_CURRENT_NAV_ID = "key_current_navigation_id"
    }
}
