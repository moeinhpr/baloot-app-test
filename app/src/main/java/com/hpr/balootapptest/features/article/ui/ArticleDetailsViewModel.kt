package com.hpr.balootapptest.features.article.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.hpr.balootapptest.core.util.ui.BaseViewModel
import com.hpr.balootapptest.features.article.domain.GetArticleDetailsLocal
import com.hpr.balootapptest.features.article.domain.GetArticleListLocal
import com.hpr.balootapptest.features.article.domain.GetArticleListRemote
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleDetailsViewModel @Inject constructor(
    private val getArticleDetailsLocal: GetArticleDetailsLocal
) : BaseViewModel() {

    private val articleId = MutableLiveData<Int>()

    val articleDetails = articleId.switchMap {
        getArticleDetailsLocal(it).asLiveData(coroutineContext)
    }

    override fun getData() {}

    fun emitArticleId(id : Int){
        if (articleId.value == null)
            articleId.value = id
    }

}
