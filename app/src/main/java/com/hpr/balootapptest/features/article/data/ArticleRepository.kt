package com.hpr.balootapptest.features.article.data

import com.hpr.balootapptest.core.exception.Exceptions
import com.hpr.balootapptest.core.model.ApiResult
import com.hpr.balootapptest.core.util.NetworkHandler
import javax.inject.Inject

class ArticleRepository @Inject constructor(
    private val articleLocalDataSource: ArticleLocalDataSource,
    private val articleRemoteDataSource: ArticleRemoteDataSource,
    private val networkHandler: NetworkHandler
) {

    fun getArticleListLocal() = articleLocalDataSource.getArticleList()

    fun getArticleDetailsLocal(id : Int) = articleLocalDataSource.getArticleDetails(id)

    suspend fun getArticleListRemote(page: Int): ApiResult<Unit> {
        return if (networkHandler.hasNetworkConnection()) {
            return when (val result = articleRemoteDataSource.getArticleList(page)) {
                is ApiResult.Success -> {
                    ApiResult.Success(
                        articleLocalDataSource.insertArticleList(
                            page,
                            result.data.articles
                        )
                    )
                }
                is ApiResult.Error -> ApiResult.Error(result.exception)
            }
        } else {
            ApiResult.Error(Exceptions.NetworkConnectionException())
        }
    }

}
