package com.hpr.balootapptest.features.article.data.network

import com.hpr.balootapptest.core.util.dateToTimeStamp
import com.hpr.balootapptest.features.article.data.entities.ArticleListEntity
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class ArticleListNetwork(
    val source : ArticleSourceNetwork = ArticleSourceNetwork(),
    val author : String = "",
    val title : String = "",
    val description : String = "",
    val urlToImage : String = "",
    val publishedAt : String = "",
    val content : String = "",
) {
    fun toArticleListEntity() =
        ArticleListEntity(
            author = author,
            title = title,
            description = description ,
            urlToImage = urlToImage,
            publishedAt = publishedAt.dateToTimeStamp(),
            content = content
        )
}
