package com.hpr.balootapptest.features.main.ui

import androidx.lifecycle.lifecycleScope
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.ui.BaseFragment
import com.hpr.balootapptest.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    override val layoutId: Int
        get() = R.layout.fragment_splash

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            delay(1000)
            navigationController.clearStack()
        }
    }
}
