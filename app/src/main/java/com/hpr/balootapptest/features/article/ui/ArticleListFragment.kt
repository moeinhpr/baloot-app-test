package com.hpr.balootapptest.features.article.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.hpr.balootapptest.R
import com.hpr.balootapptest.core.util.*
import com.hpr.balootapptest.core.util.ui.BaseFragment
import com.hpr.balootapptest.databinding.FragmentArticleListBinding
import com.hpr.balootapptest.features.main.ui.EmptyFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ArticleListFragment : BaseFragment<FragmentArticleListBinding>() {

    private val viewModel by viewModels<ArticleListViewModel>()

    private var articleListAdapter by autoCleared<ArticleListAdapter>()

    private lateinit var scrollListener: EndlessRecyclerOnScrollListener

    override val layoutId: Int
        get() = R.layout.fragment_article_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initValue()
        initView()
        initObservation()
    }

    private fun initValue() {

    }

    private fun initView() {


        binding.apply {

            networkCallback = object : NetworkCallback {
                override fun refresh() {
                    doRefresh()
                }
            }
        }

        initRecyclerView()
    }

    private fun initObservation() {

        viewModel.articleList.observe(viewLifecycleOwner, {
            if (it != null)
                articleListAdapter.submitList(it)
        })

        viewModel.networkViewState.observe(viewLifecycleOwner, {

            if (it.requestTag == ArticleRequestTag.GetArticleList.name) {

                binding.networkViewState = it

                if (it.showProgressMore)
                    showLoadingMore()

                if (it.showSuccess)
                    hideLoading()

                if (it.showError) {
                    hideLoading()
                    navigationController.showRemoteMessage(
                        it.serverErrorMessage,
                        it.errorMessage
                    )
                }


            }
        })
    }

    private fun initRecyclerView() {

        articleListAdapter = ArticleListAdapter {
            navigationController.navigateSinglePage(
                EmptyFragmentDirections.actionEmptyFragmentToArticleDetailsFragment(it.id)
            )
        }

        scrollListener = object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore(current_page: Int) {
                viewModel.getNextPage()
            }

            override fun onScrollTop(isVisible: Boolean) {}
        }

        binding.rvArticleList.apply {
            setHasFixedSize(true)
            addOnScrollListener(scrollListener)
            adapter = articleListAdapter
        }

    }

    private fun showLoadingMore() {
        articleListAdapter.setLoadingState(true)
    }

    private fun hideLoading() {

        lifecycleScope.launch(Dispatchers.Main) {
            delay(1000)
            if (viewModel.networkViewState.value?.showProgressMore != true
                && viewModel.currentPage != 1
            )
                articleListAdapter.setLoadingState(false)
        }
    }

    private fun doRefresh() {
        viewModel.refresh()
        scrollListener.refresh()
    }

}
