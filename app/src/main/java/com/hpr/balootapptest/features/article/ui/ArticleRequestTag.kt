package com.hpr.balootapptest.features.article.ui

enum class ArticleRequestTag {
    GetArticleList
}