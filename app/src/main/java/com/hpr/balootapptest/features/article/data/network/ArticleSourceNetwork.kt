package com.hpr.balootapptest.features.article.data.network

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class ArticleSourceNetwork(
    val id : String = "",
    val name : String = "",
)
