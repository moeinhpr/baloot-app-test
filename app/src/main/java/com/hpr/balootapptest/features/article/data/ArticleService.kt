package com.hpr.balootapptest.features.article.data

import com.hpr.balootapptest.core.model.BalootApiListResponse
import com.hpr.balootapptest.features.article.data.network.ArticleListNetwork
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface ArticleService {

    @GET(value = "everything")
    suspend fun getArticleList(
        @Query(value = "q", encoded = true) q: String,
        @Query(value = "from", encoded = true) from: String,
        @Query(value = "sortBy", encoded = true) sortBy: String,
        @Query(value = "apiKey", encoded = true) apiKey: String,
        @Query(value = "page", encoded = true) page: Int,
    ): Response<BalootApiListResponse<ArticleListNetwork>>

}
