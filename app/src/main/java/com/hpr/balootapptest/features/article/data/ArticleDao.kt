package com.hpr.balootapptest.features.article.data

import androidx.room.*
import com.hpr.balootapptest.features.article.data.entities.ArticleListEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ArticleDao {

    @Transaction
    @Query("SELECT * FROM ArticleListEntity ORDER BY publishedAt DESC")
    fun getArticleList(): Flow<List<ArticleListEntity>?>

    @Transaction
    @Query("SELECT * FROM ArticleListEntity WHERE id = :id")
    fun getArticleDetails(id : Int): Flow<ArticleListEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticles(articleListEntity: List<ArticleListEntity>?)

    @Query("DELETE FROM ArticleListEntity")
    suspend fun deleteArticles()
}
