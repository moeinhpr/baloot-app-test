package com.hpr.balootapptest.features.article.domain

import com.hpr.balootapptest.features.article.data.ArticleRepository
import javax.inject.Inject

class GetArticleDetailsLocal @Inject constructor(
    private val articleRepository: ArticleRepository
) {
    operator fun invoke(id: Int) = articleRepository.getArticleDetailsLocal(id)
}
