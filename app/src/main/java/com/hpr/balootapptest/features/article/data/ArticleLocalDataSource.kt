package com.hpr.balootapptest.features.article.data

import androidx.room.withTransaction
import com.hpr.balootapptest.core.db.BalootDb
import com.hpr.balootapptest.features.article.data.network.ArticleListNetwork
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticleLocalDataSource @Inject constructor(
    private val balootDb: BalootDb,
    private val articleDao: ArticleDao
) {

    fun getArticleList() = articleDao.getArticleList()

    fun getArticleDetails(id: Int) = articleDao.getArticleDetails(id)

    suspend fun insertArticleList(page: Int, articleList: List<ArticleListNetwork>) {

        val articleListEntity = articleList.map { it.toArticleListEntity() }

        balootDb.withTransaction {

            if (page == 1)
                articleDao.deleteArticles()
            articleDao.insertArticles(articleListEntity)
        }
    }

}
