package com.hpr.balootapptest.features.article.data

import com.hpr.balootapptest.core.api.BaseRemoteDataSource
import com.hpr.balootapptest.core.util.safeApiCall
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticleRemoteDataSource @Inject constructor(
    private val articleService: ArticleService
) : BaseRemoteDataSource() {

    suspend fun getArticleList(page: Int) = safeApiCall(
        call = {
            requestGetArticleList(
                page = page
            )
        },
        errorMessage = "Error get articles"
    )


    private suspend fun requestGetArticleList(
        page: Int
    ) = checkApiResult(
        articleService.getArticleList(
            page = page,
            q = "tesla",
            from = "2021-05-23",
            sortBy = "publishedAt",
            apiKey = "a7e0c72a61124f33973f61479d1f74a5"
        )
    )
}
