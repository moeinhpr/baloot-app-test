package com.hpr.balootapptest.features.article.ui

import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.hpr.balootapptest.core.util.ui.BaseViewModel
import com.hpr.balootapptest.features.article.domain.GetArticleListLocal
import com.hpr.balootapptest.features.article.domain.GetArticleListRemote
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticleListViewModel @Inject constructor(
    private val getArticleListRemote: GetArticleListRemote,
    getArticleListLocal: GetArticleListLocal
) : BaseViewModel() {

    private var _currentPage = 1
    val currentPage
        get() = _currentPage

    val articleList = getArticleListLocal().asLiveData(coroutineContext)

    init {
        getData()
    }

    override fun getData() {
        viewModelScope.launch(Dispatchers.IO) {
            if (_currentPage > 1)
                networkMoreLoading(ArticleRequestTag.GetArticleList.name)
            else networkLoading(ArticleRequestTag.GetArticleList.name)
            observeNetworkState(
                getArticleListRemote(_currentPage),
                ArticleRequestTag.GetArticleList.name
            )
        }
    }

    override fun refresh() {
        _currentPage = 1
        getData()
    }

    fun getNextPage() {
        _currentPage++
        getData()
    }
}
