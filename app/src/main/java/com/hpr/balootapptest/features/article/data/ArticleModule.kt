package com.hpr.balootapptest.features.article.data

import com.hpr.balootapptest.core.db.BalootDb
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ArticleModule {

    @Singleton
    @Provides
    fun provideArticleService(retrofit: Retrofit): ArticleService {
        return retrofit.create(ArticleService::class.java)
    }

    @Singleton
    @Provides
    fun provideArticleDao(balootDb: BalootDb): ArticleDao {
        return balootDb.articleDao()
    }
}
