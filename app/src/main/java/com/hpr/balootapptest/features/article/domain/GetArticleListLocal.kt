package com.hpr.balootapptest.features.article.domain

import com.hpr.balootapptest.features.article.data.ArticleRepository
import javax.inject.Inject

class GetArticleListLocal @Inject constructor(
    private val articleRepository: ArticleRepository
) {
    operator fun invoke() = articleRepository.getArticleListLocal()
}
