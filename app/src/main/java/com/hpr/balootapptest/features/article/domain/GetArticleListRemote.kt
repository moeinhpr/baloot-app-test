package com.hpr.balootapptest.features.article.domain

import com.hpr.balootapptest.features.article.data.ArticleRepository
import javax.inject.Inject

class GetArticleListRemote @Inject constructor(
    private val articleRepository: ArticleRepository
) {
    suspend operator fun invoke(page: Int) = articleRepository.getArticleListRemote(page)
}
